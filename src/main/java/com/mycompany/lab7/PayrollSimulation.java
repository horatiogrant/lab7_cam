/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab7;

/**
 *
 * @author grant
 */
public class PayrollSimulation {
    
    public static void main (String [] args){
        Employee bob = new Employee("bob", 15, 35);
        Employee rob = new Manager("rob", 25, 40, 100);
        bob.calculatePay();
        rob.calculatePay();
    }
}
