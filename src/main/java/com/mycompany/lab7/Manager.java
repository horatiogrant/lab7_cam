/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab7;

/**
 *
 * @author grant
 */
public class Manager extends Employee {
    double bonus;
    public Manager(String name, double wage, double hours, double bonus) {
        super(name, wage, hours);
        this.bonus=bonus;
    }
    
    @Override 
    public void calculatePay(){
        System.out.println("Employee "+getName()+"\n bonus ="+bonus+"\n total pay = "+(getWage()*getHours()+bonus));
 
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    
}
