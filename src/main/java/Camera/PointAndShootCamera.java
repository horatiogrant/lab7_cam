/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Camera;

/**
 *
 * @author grant
 */
public class PointAndShootCamera extends DigitalCamera {
    
private double ExternalMemory;

    public PointAndShootCamera(String make, String model, double megapixels, double ext) {
        super(make, model, megapixels);
        ExternalMemory = ext;
        
    }
    @Override
     public void describeCamera(){
         super.describeCamera();
         System.out.println("External Memory Space:"+ ExternalMemory+"GB");
     }
    
    
}
