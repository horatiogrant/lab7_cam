/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Camera;

/**
 *
 * @author grant
 */
public abstract class DigitalCamera {
    private String make;
    private String model;
    private double megapixels;
   
    

    public DigitalCamera(String make, String model, double megapixels) {
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
      
    }
    
 public void describeCamera(){
     
     System.out.println("Make: " +make 
             +"\n model: "+model
             +"\n megapixels: "+megapixels
     );
 }
    
}
