/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Camera;

/**
 *
 * @author grant
 */
public class DigitalCameraSimulation {
    
    public static void main (String[]args){
        
        DigitalCamera canon = new PointAndShootCamera(
                "canon", "Powershot A690",8.0,16);
        
        DigitalCamera Apple = new PhoneCamera(
        "Apple","Iphone",6.0,64);
        
        canon.describeCamera();
        Apple.describeCamera();
        
    }
    
}
