/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Camera;

/**
 *
 * @author grant
 */
public class PhoneCamera extends DigitalCamera {
     private double InternalMemory;

    public PhoneCamera(String make, String model, double megapixels,double InternalMemory) {
        super(make, model, megapixels);
        this.InternalMemory = InternalMemory;
    }
     @Override
     public void describeCamera(){
         super.describeCamera();
         System.out.println("Internal Memory Space:"+ InternalMemory+"GB");
     }
}
